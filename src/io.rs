/*
    This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy
    of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.

    Copyright 2019 Gert Dewit <gert@hobbiton.be>
*/
use std::io;
use failure::Error;
use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};

#[derive(Debug, Fail, PartialEq)]
enum IoError {
    #[fail(display = "Short read")]
    ShortRead,
}

/// Read a binary string using the format described in rfc4251
///
/// Arbitrary length binary string.  Strings are allowed to contain arbitrary binary data,
/// including null characters and 8-bit characters.  They are stored as a uint32 containing its
/// length (number of bytes that follow) and zero (= empty string) or more bytes that are the value
/// of the string.  Terminating null characters are not used
///
/// # Examples
///
/// Read a 4 byte value:
///
/// ```rust
/// use secops::io::ReadSizedBytesExt;
/// use std::io::Cursor;
///
/// let mut reader = Cursor::new(vec![0x00, 0x00, 0x00, 0x04, 0xf0, 0x9f, 0xa6, 0x80]);
///
/// let bytes = reader.read_sized().unwrap();
/// assert_eq!(4, bytes.len());
/// assert_eq!("🦀", String::from_utf8(bytes).unwrap());
/// ```
pub trait ReadSizedBytesExt: io::Read {
    fn read_sized(&mut self) -> Result<Vec<u8>, Error> {
        let size = self.read_u32::<NetworkEndian>()? as usize;
        let mut input: Vec<u8> = vec![0; size];
        if size == self.read(&mut input)? {
            return Ok(input);
        }
        Err(IoError::ShortRead)?
    }
}

impl<R: io::Read + ?Sized> ReadSizedBytesExt for R {}

/// Write a binary string using the format described in rfc4251
///
/// Arbitrary length binary string.  Strings are allowed to contain arbitrary binary data,
/// including null characters and 8-bit characters.  They are stored as a uint32 containing its
/// length (number of bytes that follow) and zero (= empty string) or more bytes that are the value
/// of the string.  Terminating null characters are not used
///
/// # Examples
///
/// Write a 4 byte value:
///
/// ```rust
/// use secops::io::WriteSizedBytesExt;
/// use std::io::Cursor;
///
/// let mut writer = Cursor::new(vec![]);
///
/// let len = writer.write_sized("🦀".as_bytes()).unwrap();
/// assert_eq!(8, len);
/// assert_eq!(vec![0x00, 0x00, 0x00, 0x04, 0xf0, 0x9f, 0xa6, 0x80], writer.into_inner());
/// ```
pub trait WriteSizedBytesExt: io::Write {
    fn write_sized(&mut self, bytes: &[u8]) -> Result<usize, Error> {
        self.write_u32::<NetworkEndian>(bytes.len() as u32)?;
        self.write_all(bytes)?;
        Ok(bytes.len() + 4)
    }
}

impl<W: io::Write + ?Sized> WriteSizedBytesExt for W {}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    #[test]
    fn read_sized_ok() {
        let one_input = &vec![0, 0, 0, 1, 127];
        let mut one_stream = Cursor::new(one_input);
        let one = one_stream.read_sized().unwrap();
        assert_eq!(1, one.len());
        assert_eq!(127, one[0]);
        let two_input = &vec![0, 0, 0, 2, 127, 255];
        let mut two_stream = Cursor::new(two_input);
        let two = two_stream.read_sized().unwrap();
        assert_eq!(2, two.len());
        assert_eq!(127, two[0]);
        assert_eq!(255, two[1]);
    }

    #[test]
    fn read_sized_concatenated() {
        let input = &vec![0, 0, 0, 1, 127, 0, 0, 0, 2, 127, 255];
        let mut stream = Cursor::new(input);
        let one = stream.read_sized().unwrap();
        assert_eq!(1, one.len());
        assert_eq!(127, one[0]);
        let two = stream.read_sized().unwrap();
        assert_eq!(2, two.len());
        assert_eq!(127, two[0]);
        assert_eq!(255, two[1]);
    }

    #[test]
    fn read_sized_bytes_short_size() {
        let mut stream_one = Cursor::new(vec![0]);
        assert!(stream_one.read_sized().is_err());
        let mut stream_two = Cursor::new(vec![0, 0]);
        assert!(stream_two.read_sized().is_err());
        let mut stream_three = Cursor::new(vec![0, 0, 0]);
        assert!(stream_three.read_sized().is_err());
    }

    #[test]
    fn read_sized_bytes_zero_len() {
        let mut stream = Cursor::new(vec![0, 0, 0, 0]);
        assert_eq!(0, stream.read_sized().unwrap().len())
    }

    #[test]
    fn read_sized_bytes_short_data() {
        let mut stream_one = Cursor::new(vec![0, 0, 0, 1]);
        assert!(stream_one.read_sized().is_err());
        let mut stream_two = Cursor::new(vec![0, 0, 0, 2, 254]);
        assert!(stream_two.read_sized().is_err());
    }

    #[test]
    fn write_sized_ok() {
        let dest: Vec<u8> = Vec::new();
        let mut stream = Cursor::new(dest);
        assert_eq!(5, stream.write_sized(vec![11].as_slice()).unwrap());
        assert_eq!(6, stream.write_sized(vec![22, 23].as_slice()).unwrap());
        assert_eq!(vec![0, 0, 0, 1, 11, 0, 0, 0, 2, 22, 23], stream.into_inner());
    }

    #[test]
    fn write_sized_zero_len() {
        let dest: Vec<u8> = Vec::new();
        let mut stream = Cursor::new(dest);
        assert_eq!(4, stream.write_sized(vec![].as_slice()).unwrap());
        assert_eq!(vec![0, 0, 0, 0], stream.into_inner());
    }
}