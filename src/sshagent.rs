/*
    This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy
    of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.

    Copyright 2019 Gert Dewit <gert@hobbiton.be>
*/
use std::env;
use std::io::Read;
use std::io::Write;
use std::io::Cursor;
use std::os::unix::net::UnixStream;
use failure::Error;
use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
use crate::io::{ReadSizedBytesExt, WriteSizedBytesExt};
use sha2::{Sha256, Digest};
use md5::Md5;


pub const DEFAULT_SSH_AUTH_SOCK_ENV_VARIABLE_NAME: &'static str = "SSH_AUTH_SOCK";
const RSA_TYPE_NAME: &'static str = "ssh-rsa";
const ED25519_TYPE_NAME: &'static str = "ssh-ed25519";
const ECDSA_TYPE_NAME: &'static str = "ecdsa-sha2-nistp256";
const DSA_TYPE_NAME: &'static str = "ssh-dss";

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Fail, PartialEq)]
enum AgentConnectionError {
    #[fail(display = "SSH Agent replied with an unexpected message type")]
    SshAgentUnexpectedReply,
    #[fail(display = "SSH Agent short reply message")]
    SshAgentShortReply,
}

#[derive(Debug, Fail, PartialEq)]
enum AgentClientError {
    #[fail(display = "SSH key not found")]
    SshAgentKeyNotFound,
}

#[derive(Debug, PartialEq)]
pub enum SshKeyType {
    UNKNOWN,
    DSA,
    ECDSA,
    ED25519,
    RSA,
}

impl SshKeyType {
    fn from(type_string: &str) -> Self {
        match type_string {
            RSA_TYPE_NAME => SshKeyType::RSA,
            ED25519_TYPE_NAME => SshKeyType::ED25519,
            ECDSA_TYPE_NAME => SshKeyType::ECDSA,
            DSA_TYPE_NAME => SshKeyType::DSA,
            _ => SshKeyType::UNKNOWN
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct SshKey {
    pub key_type: SshKeyType,
    pub comment: String,
    pub public: Vec<u8>,
    private: Option<Vec<u8>>,
}

impl SshKey {
    pub fn new(key_type: SshKeyType, comment: String, public: Vec<u8>, private: Option<Vec<u8>>) -> Self {
        SshKey {
            key_type,
            comment,
            public,
            private,
        }
    }

    fn read(mut stream: impl Read) -> Result<Self> {
        let raw_key = stream.read_sized()?;
        let mut key_stream = Cursor::new(&raw_key);
        Ok(SshKey {
            key_type: SshKeyType::from(String::from_utf8(key_stream.read_sized()?)?.as_str()),
            comment: String::from_utf8(stream.read_sized()?)?,
            public: raw_key,
            private: None,
        })
    }

    pub fn sha256_fingerprint(& self) -> Vec<u8> {
        // vec![0]
        let mut sha256 = Sha256::default();
        sha256.update(&self.public);
        sha256.finalize().to_vec()
    }

    pub fn md5_fingerprint(& self) -> Vec<u8> {
        let mut md5 = Md5::default();
        md5.update(&self.public);
        md5.finalize().to_vec()
    }
}

#[derive(Debug, PartialEq)]
pub struct SshSignature {
    pub signature_type: SshKeyType,
    pub signature: Vec<u8>,
}

impl SshSignature {
    fn read(mut stream: impl Read) -> Result<Self> {
        let sig_type = stream.read_sized()?;
        let signature = stream.read_sized()?;
        Ok(SshSignature {
            signature_type: SshKeyType::from(String::from_utf8(sig_type)?.as_str()),
            signature,
        })
    }
}

#[allow(dead_code)]
#[repr(u8)]
#[derive(Debug, PartialEq, Copy, Clone)]
enum SshAgentRequestTypes {
    SshAgentClientRequestIdentities = 11,
    SshAgentClientSignRequest = 13,
    SshAgentClientAddIdentity = 17,
    SshAgentClientRemoveIdentity = 18,
    SshAgentClientRemoveAllIdentities = 19,
    SshAgentClientAddIdConstrained = 25,
    SshAgentClientAddSmartcardKey = 20,
    SshAgentClientRemoveSmartcardKey = 21,
    SshAgentClientLock = 22,
    SshAgentClientUnlock = 23,
    SshAgentClientAddSmartcardKeyConstrained = 26,
    SshAgentClientExtension = 27,
}

pub struct SshAgentRequestMessage {
    message_type: SshAgentRequestTypes,
    payload: Option<Vec<u8>>,
}

impl SshAgentRequestMessage {
    fn new(message_type: SshAgentRequestTypes, payload: Option<Vec<u8>>) -> SshAgentRequestMessage {
        SshAgentRequestMessage {
            message_type,
            payload,
        }
    }
}

impl Into<Vec<u8>> for SshAgentRequestMessage {
    fn into(self) -> Vec<u8> {
        let vec = Vec::new();
        let msg_len: u32 = match &self.payload {
            Some(payload) => payload.len() as u32 + 1,
            None => 1
        };
        let mut cursor = Cursor::new(vec);
        cursor.write_u32::<NetworkEndian>(msg_len);
        cursor.write_u8(self.message_type as u8);
        if let Some(payload) = self.payload {
            cursor.write(&payload);
        }
        cursor.into_inner()
    }
}

#[allow(dead_code)]
#[repr(u8)]
#[derive(Debug, PartialEq, Copy, Clone)]
enum SshAgentReplyTypes {
    SshAgentFailure = 5,
    SshAgentSuccess = 6,
    SshAgentExtensionFailure = 28,
    SshAgentIdentitiesAnswer = 12,
    SshAgentSignResponse = 14,
}

struct SshAgentReplyMessage {
    payload: Vec<u8>,
}

#[derive(Debug, PartialEq)]
pub enum KeySelector {
    Comment(String),
    SHA256Fingerprint(Vec<u8>),
    MD5Fingerprint(Vec<u8>),
}

struct SshKeyStore {
    keys: Option<Vec<SshKey>>
}

impl SshKeyStore {
    pub fn new(keys: Option<Vec<SshKey>>) -> Self {
        SshKeyStore {
            keys
        }
    }

    pub fn find_key(&self, selector: &KeySelector) -> Option<&SshKey> {
        match &self.keys {
            Some(keys) => {
                for key in keys {
                    match selector {
                        KeySelector::Comment(ref comment) => {
                            if key.comment == *comment {
                                return Some(key);
                            }
                        }
                        KeySelector::MD5Fingerprint(digest) => {
                            if &key.md5_fingerprint() == digest {
                                return Some(key);
                            }
                        }
                        KeySelector::SHA256Fingerprint(digest) => {
                            if &key.sha256_fingerprint() == digest {
                                return Some(key);
                            }
                        }
                    }
                }
                None
            }
            None => None
        }
    }
}

pub struct SshAgentClient {
    connection: UnixStream,
    keys: SshKeyStore,
}

/// SSH Agent Client, connects using a Unix Domain Socket set using the standard Open SSH Agent environment variable
impl SshAgentClient {
    pub fn connect() -> Result<SshAgentClient> {
        let mut connection = UnixStream::connect(env::var(DEFAULT_SSH_AUTH_SOCK_ENV_VARIABLE_NAME)?)?;
        let keys = SshKeyStore::new(Some(SshAgentClient::list_keys_internal(&mut connection)?));
        Ok(SshAgentClient {
            connection,
            keys,
        })
    }

    /// Sign a message with a chosen key
    pub fn sign(&mut self, data: &[u8], selector: KeySelector) -> Result<SshSignature> {
        match self.keys.find_key(&selector) {
            Some(key) => {
                let message: Vec<u8> = SshAgentClient::sign_message(data, key)?.into();
                let _num_written = self.connection.write_all(message.as_slice());
                SshAgentClient::read_signed(&self.connection)
            }
            None => Err(AgentClientError::SshAgentKeyNotFound)?
        }
    }

    fn sign_message(data: &[u8], key: &SshKey) -> Result<SshAgentRequestMessage> {
        let vec = Vec::new();
        let mut cursor = Cursor::new(vec);
        cursor.write_sized(key.public.as_slice())?;
        cursor.write_sized(data)?;
        cursor.write_u32::<NetworkEndian>(0)?; // flags, no flags supported yet
        Ok(SshAgentRequestMessage::new(SshAgentRequestTypes::SshAgentClientSignRequest, Some(cursor.into_inner())))
    }

    fn read_signed(stream: impl Read) -> Result<SshSignature> {
        let signed_message = SshAgentClient::read_reply(SshAgentReplyTypes::SshAgentSignResponse, stream)?;
        let raw_signature = Cursor::new(signed_message.payload).read_sized()?;
        Ok(SshSignature::read(Cursor::new(raw_signature))?)
    }

    /// List all keys known to the SSH Agent
    pub fn list_keys(&mut self) -> Result<Vec<SshKey>> {
        SshAgentClient::list_keys_internal(&mut self.connection)
    }

    fn list_keys_internal(stream: &mut UnixStream) -> Result<Vec<SshKey>> {
        let list_message: Vec<u8> = SshAgentClient::list_message().into();
        let _num_written = stream.write_all(list_message.as_slice());
        SshAgentClient::read_keys(stream)
    }

    fn list_message() -> SshAgentRequestMessage {
        SshAgentRequestMessage::new(SshAgentRequestTypes::SshAgentClientRequestIdentities, None)
    }

    fn read_keys(stream: impl Read) -> Result<Vec<SshKey>> {
        let list_keys_message = SshAgentClient::read_reply(SshAgentReplyTypes::SshAgentIdentitiesAnswer, stream)?;
        let mut message_stream = Cursor::new(list_keys_message.payload);
        let num_keys = message_stream.read_u32::<NetworkEndian>()? as usize;
        let mut keys = Vec::new();
        for _n in 0..num_keys {
            keys.push(SshKey::read(&mut message_stream)?);
        }
        Ok(keys)
    }

    fn read_reply(reply_type: SshAgentReplyTypes, mut stream: impl Read) -> Result<SshAgentReplyMessage> {
        let message_size = stream.read_u32::<NetworkEndian>()? as usize;
        let mut input: Vec<u8> = vec![0; message_size];
        if message_size == stream.read(&mut input)? {
            if input[0] == reply_type as u8 {
                Ok(SshAgentReplyMessage {
                    payload: input[1..].to_vec(),
                })
            } else {
                Err(AgentConnectionError::SshAgentUnexpectedReply)?
            }
        } else {
            Err(AgentConnectionError::SshAgentShortReply)?
        }
    }
}

#[cfg(test)]
#[cfg(not(tarpaulin_include))]
mod tests {
    use super::*;
    use std::io::Cursor;
    use crate::sshagent::SshAgentReplyTypes::{SshAgentFailure, SshAgentSignResponse, SshAgentSuccess, SshAgentIdentitiesAnswer};
    use core::borrow::Borrow;

    const RSA_COMMENT: &'static str = "secops@rsa.hobbiton.be";
    const RSA_KEY: [u8; 279] = [0, 0, 0, 7, 115, 115, 104, 45, 114, 115, 97, 0, 0, 0, 3, 1, 0, 1, 0, 0, 1, 1, 0, 188, 183, 121, 206, 246, 102, 197, 207, 243, 253, 142, 210, 82, 47, 78, 170, 246, 186, 100, 129, 165, 223, 157, 61, 54, 168, 180, 188, 37, 110, 200, 109, 4, 51, 110, 165, 73, 90, 142, 228, 112, 44, 103, 199, 224, 62, 2, 16, 236, 209, 34, 255, 43, 69, 30, 49, 92, 156, 209, 84, 5, 79, 22, 95, 96, 188, 122, 213, 189, 186, 101, 143, 98, 29, 163, 67, 38, 52, 210, 219, 214, 254, 217, 174, 206, 101, 28, 205, 192, 91, 95, 84, 45, 74, 124, 90, 55, 227, 129, 10, 84, 116, 102, 205, 229, 68, 184, 144, 65, 231, 191, 14, 76, 115, 141, 123, 210, 219, 182, 227, 167, 103, 48, 176, 197, 247, 108, 218, 192, 9, 48, 138, 216, 62, 28, 33, 215, 125, 202, 121, 84, 252, 37, 5, 175, 134, 78, 31, 161, 116, 240, 97, 138, 30, 46, 9, 237, 160, 174, 56, 251, 219, 158, 205, 232, 115, 59, 92, 127, 68, 85, 133, 162, 217, 108, 45, 34, 174, 100, 213, 160, 26, 205, 169, 213, 245, 110, 13, 204, 10, 123, 153, 237, 216, 39, 92, 180, 137, 38, 43, 129, 54, 234, 173, 157, 8, 223, 141, 96, 237, 78, 179, 153, 59, 213, 26, 148, 59, 151, 78, 92, 43, 14, 181, 83, 253, 121, 5, 197, 227, 239, 117, 97, 199, 242, 125, 234, 234, 121, 56, 66, 203, 122, 123, 168, 144, 29, 163, 247, 27, 97, 158, 189, 39, 136, 169];
    const RSA_MD5: [u8; 16] = [0xc0, 0x71, 0xa7, 0x3c, 0xec, 0x5d, 0x23, 0xfb, 0xe6, 0x02, 0x6e, 0xbf, 0x71, 0xd4, 0xdc, 0xb8];
    const ED25519_COMMENT: &'static str = "secops@ed25519.hobbiton.be";
    const ED25519_KEY: [u8; 51] = [0, 0, 0, 11, 115, 115, 104, 45, 101, 100, 50, 53, 53, 49, 57, 0, 0, 0, 32, 166, 140, 161, 199, 166, 96, 91, 226, 54, 172, 61, 2, 251, 40, 180, 140, 73, 49, 148, 14, 75, 181, 196, 138, 85, 176, 210, 243, 175, 55, 184, 246];
    const ED25519_MD5: [u8; 16] = [0x6d, 0xb3, 0x35, 0xf0, 0x5b, 0x50, 0x97, 0x36, 0xc0, 0xfb, 0x46, 0xdb, 0x01, 0xcd, 0xa2, 0x53];
    const ED25519_SHA256: [u8; 32] = [0x3f, 0x36, 0x45, 0x73, 0x2e, 0xbe, 0x31, 0x8c, 0xdb, 0x97, 0x86, 0x0a, 0xdf, 0x4c, 0x69, 0x3c, 0xdf, 0x40, 0xb5, 0xba, 0x52, 0x9d, 0x11, 0x80, 0x99, 0x87, 0xe6, 0x75, 0x01, 0x84, 0xe1, 0x3d];
    const DSA_COMMENT: &'static str = "secops@dsa.hobbiton.be";
    const DSA_KEY: [u8; 433] = [0, 0, 0, 7, 115, 115, 104, 45, 100, 115, 115, 0, 0, 0, 129, 0, 244, 143, 220, 103, 81, 216, 43, 125, 24, 130, 14, 40, 147, 186, 43, 22, 44, 124, 161, 32, 232, 58, 16, 184, 205, 163, 89, 14, 80, 28, 135, 109, 210, 192, 251, 76, 98, 188, 24, 150, 169, 25, 74, 182, 2, 183, 27, 43, 147, 14, 135, 92, 246, 17, 97, 19, 147, 230, 235, 175, 252, 53, 239, 71, 254, 65, 25, 188, 200, 18, 8, 191, 95, 231, 93, 10, 16, 38, 203, 127, 181, 7, 184, 177, 12, 54, 147, 213, 234, 112, 223, 127, 3, 170, 85, 68, 63, 206, 78, 31, 130, 170, 158, 37, 157, 44, 154, 73, 88, 94, 151, 160, 140, 215, 148, 165, 80, 96, 124, 185, 129, 226, 95, 243, 194, 126, 238, 181, 0, 0, 0, 21, 0, 184, 240, 53, 240, 90, 240, 230, 125, 134, 141, 102, 234, 30, 253, 79, 204, 115, 9, 87, 137, 0, 0, 0, 128, 96, 119, 19, 152, 164, 165, 165, 12, 45, 195, 160, 125, 169, 255, 62, 111, 156, 119, 198, 40, 240, 56, 238, 113, 156, 162, 225, 168, 131, 88, 100, 135, 48, 242, 78, 204, 110, 233, 118, 79, 48, 180, 17, 184, 36, 237, 174, 97, 233, 77, 75, 153, 156, 108, 254, 235, 220, 157, 154, 171, 171, 193, 77, 159, 53, 186, 48, 27, 184, 39, 140, 213, 224, 109, 195, 42, 61, 38, 254, 169, 130, 12, 155, 5, 99, 38, 27, 237, 83, 77, 121, 135, 63, 251, 244, 39, 137, 233, 114, 43, 0, 148, 208, 127, 162, 251, 128, 26, 83, 220, 140, 157, 244, 24, 74, 38, 132, 196, 215, 46, 201, 109, 234, 218, 174, 70, 107, 205, 0, 0, 0, 128, 18, 101, 111, 140, 166, 138, 220, 175, 118, 141, 129, 105, 165, 57, 41, 209, 252, 144, 9, 103, 122, 16, 64, 39, 23, 189, 127, 47, 172, 150, 139, 253, 238, 53, 226, 17, 110, 57, 138, 15, 39, 7, 117, 233, 141, 224, 110, 52, 3, 112, 70, 174, 95, 94, 177, 212, 252, 193, 71, 8, 128, 219, 152, 152, 125, 244, 122, 199, 236, 253, 142, 86, 141, 158, 157, 163, 150, 24, 225, 163, 241, 54, 208, 218, 154, 241, 200, 74, 166, 16, 185, 106, 83, 18, 105, 230, 250, 255, 208, 202, 113, 172, 145, 94, 212, 16, 209, 220, 239, 117, 139, 242, 161, 179, 90, 176, 18, 237, 27, 60, 84, 143, 92, 211, 167, 104, 130, 85];
    const ECDSA_COMMENT: &'static str = "secops@ecdsa.hobbiton.be";
    const ECDSA__KEY: [u8; 104] = [0, 0, 0, 19, 101, 99, 100, 115, 97, 45, 115, 104, 97, 50, 45, 110, 105, 115, 116, 112, 50, 53, 54, 0, 0, 0, 8, 110, 105, 115, 116, 112, 50, 53, 54, 0, 0, 0, 65, 4, 70, 123, 72, 228, 99, 98, 53, 111, 170, 0, 239, 199, 185, 42, 49, 58, 65, 222, 0, 32, 58, 234, 212, 51, 194, 189, 150, 246, 42, 129, 197, 244, 67, 113, 84, 45, 39, 3, 77, 247, 105, 184, 251, 236, 118, 19, 94, 2, 68, 174, 14, 23, 170, 228, 32, 221, 95, 14, 163, 211, 187, 249, 107, 157];
    const KEYS_LIST: [u8; 1002] = [0, 0, 3, 230, SshAgentIdentitiesAnswer as u8, 0, 0, 0, 4, 0, 0, 1, 177, 0, 0, 0, 7, 115, 115, 104, 45, 100, 115, 115, 0, 0, 0, 129, 0, 244, 143, 220, 103, 81, 216, 43, 125, 24, 130, 14, 40, 147, 186, 43, 22, 44, 124, 161, 32, 232, 58, 16, 184, 205, 163, 89, 14, 80, 28, 135, 109, 210, 192, 251, 76, 98, 188, 24, 150, 169, 25, 74, 182, 2, 183, 27, 43, 147, 14, 135, 92, 246, 17, 97, 19, 147, 230, 235, 175, 252, 53, 239, 71, 254, 65, 25, 188, 200, 18, 8, 191, 95, 231, 93, 10, 16, 38, 203, 127, 181, 7, 184, 177, 12, 54, 147, 213, 234, 112, 223, 127, 3, 170, 85, 68, 63, 206, 78, 31, 130, 170, 158, 37, 157, 44, 154, 73, 88, 94, 151, 160, 140, 215, 148, 165, 80, 96, 124, 185, 129, 226, 95, 243, 194, 126, 238, 181, 0, 0, 0, 21, 0, 184, 240, 53, 240, 90, 240, 230, 125, 134, 141, 102, 234, 30, 253, 79, 204, 115, 9, 87, 137, 0, 0, 0, 128, 96, 119, 19, 152, 164, 165, 165, 12, 45, 195, 160, 125, 169, 255, 62, 111, 156, 119, 198, 40, 240, 56, 238, 113, 156, 162, 225, 168, 131, 88, 100, 135, 48, 242, 78, 204, 110, 233, 118, 79, 48, 180, 17, 184, 36, 237, 174, 97, 233, 77, 75, 153, 156, 108, 254, 235, 220, 157, 154, 171, 171, 193, 77, 159, 53, 186, 48, 27, 184, 39, 140, 213, 224, 109, 195, 42, 61, 38, 254, 169, 130, 12, 155, 5, 99, 38, 27, 237, 83, 77, 121, 135, 63, 251, 244, 39, 137, 233, 114, 43, 0, 148, 208, 127, 162, 251, 128, 26, 83, 220, 140, 157, 244, 24, 74, 38, 132, 196, 215, 46, 201, 109, 234, 218, 174, 70, 107, 205, 0, 0, 0, 128, 18, 101, 111, 140, 166, 138, 220, 175, 118, 141, 129, 105, 165, 57, 41, 209, 252, 144, 9, 103, 122, 16, 64, 39, 23, 189, 127, 47, 172, 150, 139, 253, 238, 53, 226, 17, 110, 57, 138, 15, 39, 7, 117, 233, 141, 224, 110, 52, 3, 112, 70, 174, 95, 94, 177, 212, 252, 193, 71, 8, 128, 219, 152, 152, 125, 244, 122, 199, 236, 253, 142, 86, 141, 158, 157, 163, 150, 24, 225, 163, 241, 54, 208, 218, 154, 241, 200, 74, 166, 16, 185, 106, 83, 18, 105, 230, 250, 255, 208, 202, 113, 172, 145, 94, 212, 16, 209, 220, 239, 117, 139, 242, 161, 179, 90, 176, 18, 237, 27, 60, 84, 143, 92, 211, 167, 104, 130, 85, 0, 0, 0, 22, 115, 101, 99, 111, 112, 115, 64, 100, 115, 97, 46, 104, 111, 98, 98, 105, 116, 111, 110, 46, 98, 101, 0, 0, 0, 104, 0, 0, 0, 19, 101, 99, 100, 115, 97, 45, 115, 104, 97, 50, 45, 110, 105, 115, 116, 112, 50, 53, 54, 0, 0, 0, 8, 110, 105, 115, 116, 112, 50, 53, 54, 0, 0, 0, 65, 4, 70, 123, 72, 228, 99, 98, 53, 111, 170, 0, 239, 199, 185, 42, 49, 58, 65, 222, 0, 32, 58, 234, 212, 51, 194, 189, 150, 246, 42, 129, 197, 244, 67, 113, 84, 45, 39, 3, 77, 247, 105, 184, 251, 236, 118, 19, 94, 2, 68, 174, 14, 23, 170, 228, 32, 221, 95, 14, 163, 211, 187, 249, 107, 157, 0, 0, 0, 24, 115, 101, 99, 111, 112, 115, 64, 101, 99, 100, 115, 97, 46, 104, 111, 98, 98, 105, 116, 111, 110, 46, 98, 101, 0, 0, 0, 51, 0, 0, 0, 11, 115, 115, 104, 45, 101, 100, 50, 53, 53, 49, 57, 0, 0, 0, 32, 166, 140, 161, 199, 166, 96, 91, 226, 54, 172, 61, 2, 251, 40, 180, 140, 73, 49, 148, 14, 75, 181, 196, 138, 85, 176, 210, 243, 175, 55, 184, 246, 0, 0, 0, 26, 115, 101, 99, 111, 112, 115, 64, 101, 100, 50, 53, 53, 49, 57, 46, 104, 111, 98, 98, 105, 116, 111, 110, 46, 98, 101, 0, 0, 1, 23, 0, 0, 0, 7, 115, 115, 104, 45, 114, 115, 97, 0, 0, 0, 3, 1, 0, 1, 0, 0, 1, 1, 0, 188, 183, 121, 206, 246, 102, 197, 207, 243, 253, 142, 210, 82, 47, 78, 170, 246, 186, 100, 129, 165, 223, 157, 61, 54, 168, 180, 188, 37, 110, 200, 109, 4, 51, 110, 165, 73, 90, 142, 228, 112, 44, 103, 199, 224, 62, 2, 16, 236, 209, 34, 255, 43, 69, 30, 49, 92, 156, 209, 84, 5, 79, 22, 95, 96, 188, 122, 213, 189, 186, 101, 143, 98, 29, 163, 67, 38, 52, 210, 219, 214, 254, 217, 174, 206, 101, 28, 205, 192, 91, 95, 84, 45, 74, 124, 90, 55, 227, 129, 10, 84, 116, 102, 205, 229, 68, 184, 144, 65, 231, 191, 14, 76, 115, 141, 123, 210, 219, 182, 227, 167, 103, 48, 176, 197, 247, 108, 218, 192, 9, 48, 138, 216, 62, 28, 33, 215, 125, 202, 121, 84, 252, 37, 5, 175, 134, 78, 31, 161, 116, 240, 97, 138, 30, 46, 9, 237, 160, 174, 56, 251, 219, 158, 205, 232, 115, 59, 92, 127, 68, 85, 133, 162, 217, 108, 45, 34, 174, 100, 213, 160, 26, 205, 169, 213, 245, 110, 13, 204, 10, 123, 153, 237, 216, 39, 92, 180, 137, 38, 43, 129, 54, 234, 173, 157, 8, 223, 141, 96, 237, 78, 179, 153, 59, 213, 26, 148, 59, 151, 78, 92, 43, 14, 181, 83, 253, 121, 5, 197, 227, 239, 117, 97, 199, 242, 125, 234, 234, 121, 56, 66, 203, 122, 123, 168, 144, 29, 163, 247, 27, 97, 158, 189, 39, 136, 169, 0, 0, 0, 22, 115, 101, 99, 111, 112, 115, 64, 114, 115, 97, 46, 104, 111, 98, 98, 105, 116, 111, 110, 46, 98, 101];
    const ED25519_RAW_KEY: [u8; 85] = [0, 0, 0, 51, 0, 0, 0, 11, 115, 115, 104, 45, 101, 100, 50, 53, 53, 49, 57, 0, 0, 0, 32, 166, 140, 161, 199, 166, 96, 91, 226, 54, 172, 61, 2, 251, 40, 180, 140, 73, 49, 148, 14, 75, 181, 196, 138, 85, 176, 210, 243, 175, 55, 184, 246, 0, 0, 0, 26, 115, 101, 99, 111, 112, 115, 64, 101, 100, 50, 53, 53, 49, 57, 46, 104, 111, 98, 98, 105, 116, 111, 110, 46, 98, 101];
    const ONE_KEY_LIST: [u8; 94] = [0, 0, 0, 90, SshAgentIdentitiesAnswer as u8, 0, 0, 0, 1, 0, 0, 0, 51, 0, 0, 0, 11, 115, 115, 104, 45, 101, 100, 50, 53, 53, 49, 57, 0, 0, 0, 32, 166, 140, 161, 199, 166, 96, 91, 226, 54, 172, 61, 2, 251, 40, 180, 140, 73, 49, 148, 14, 75, 181, 196, 138, 85, 176, 210, 243, 175, 55, 184, 246, 0, 0, 0, 26, 115, 101, 99, 111, 112, 115, 64, 101, 100, 50, 53, 53, 49, 57, 46, 104, 111, 98, 98, 105, 116, 111, 110, 46, 98, 101];

    const RSA_SIGNATURE_RESP: [u8; 536] = [0, 0, 2, 20, SshAgentSignResponse as u8, 0, 0, 2, 15, 0, 0, 0, 7, 115, 115, 104, 45, 114, 115, 97, 0, 0, 2, 0, 88, 235, 40, 202, 35, 212, 89, 232, 237, 242, 50, 0, 172, 134, 70, 167, 106, 138, 211, 103, 71, 215, 158, 107, 248, 53, 255, 185, 177, 75, 9, 221, 31, 62, 240, 38, 20, 179, 147, 129, 198, 229, 25, 3, 59, 105, 109, 137, 157, 25, 58, 218, 184, 123, 25, 232, 12, 112, 232, 104, 44, 201, 130, 81, 209, 168, 55, 2, 65, 36, 75, 126, 131, 170, 76, 241, 194, 118, 197, 120, 178, 75, 231, 217, 3, 144, 160, 232, 174, 80, 96, 104, 171, 251, 9, 6, 211, 16, 50, 185, 141, 140, 137, 239, 232, 104, 117, 24, 108, 83, 130, 56, 185, 48, 13, 160, 145, 39, 59, 169, 154, 154, 101, 84, 15, 162, 222, 183, 225, 111, 140, 13, 75, 198, 96, 19, 175, 64, 213, 150, 96, 191, 208, 157, 140, 83, 76, 105, 191, 117, 61, 64, 192, 155, 219, 229, 26, 34, 142, 48, 219, 57, 168, 123, 38, 80, 96, 60, 44, 38, 10, 143, 120, 210, 95, 240, 216, 133, 85, 146, 178, 145, 194, 72, 159, 130, 119, 132, 14, 56, 58, 174, 47, 102, 216, 34, 211, 50, 191, 247, 216, 97, 212, 92, 171, 62, 230, 118, 67, 250, 148, 45, 60, 178, 212, 232, 119, 20, 243, 107, 229, 234, 115, 246, 212, 93, 83, 161, 49, 188, 0, 252, 87, 200, 5, 27, 235, 101, 117, 99, 210, 24, 144, 53, 185, 143, 206, 176, 112, 204, 39, 56, 130, 40, 71, 49, 6, 190, 254, 191, 187, 137, 211, 139, 19, 179, 165, 164, 215, 191, 244, 151, 237, 11, 17, 103, 37, 242, 105, 252, 188, 148, 29, 152, 130, 89, 156, 44, 150, 162, 46, 39, 90, 210, 7, 194, 211, 227, 28, 165, 48, 53, 179, 90, 215, 220, 44, 143, 45, 115, 127, 9, 122, 154, 31, 206, 44, 198, 136, 8, 132, 224, 185, 232, 17, 19, 23, 39, 238, 107, 57, 33, 219, 64, 42, 20, 38, 181, 39, 24, 101, 182, 183, 163, 127, 184, 90, 113, 218, 137, 255, 197, 34, 142, 33, 130, 82, 225, 7, 214, 158, 26, 158, 89, 171, 94, 193, 173, 48, 189, 74, 214, 143, 56, 73, 131, 16, 251, 28, 3, 64, 69, 251, 181, 209, 239, 186, 39, 114, 139, 192, 235, 41, 71, 37, 101, 233, 144, 93, 195, 66, 18, 101, 226, 93, 74, 202, 52, 7, 36, 48, 39, 203, 104, 136, 111, 120, 139, 10, 164, 164, 157, 70, 235, 29, 248, 253, 18, 14, 129, 233, 216, 176, 74, 189, 183, 103, 223, 146, 48, 10, 243, 185, 68, 63, 250, 103, 15, 81, 182, 74, 244, 247, 142, 182, 216, 224, 39, 55, 254, 129, 76, 174, 96, 12, 10, 154, 209, 135, 232, 64, 125, 114, 6, 158, 227, 84, 92, 49, 13, 232, 114, 165, 152, 216, 51, 129, 58, 15, 211, 30, 83, 230, 13, 125, 231, 1, 167, 193, 63, 250, 56, 133, 208, 246, 70, 24, 133, 234, 179, 81, 248];
    const RSA_SIGNATURE: [u8; 512] = [88, 235, 40, 202, 35, 212, 89, 232, 237, 242, 50, 0, 172, 134, 70, 167, 106, 138, 211, 103, 71, 215, 158, 107, 248, 53, 255, 185, 177, 75, 9, 221, 31, 62, 240, 38, 20, 179, 147, 129, 198, 229, 25, 3, 59, 105, 109, 137, 157, 25, 58, 218, 184, 123, 25, 232, 12, 112, 232, 104, 44, 201, 130, 81, 209, 168, 55, 2, 65, 36, 75, 126, 131, 170, 76, 241, 194, 118, 197, 120, 178, 75, 231, 217, 3, 144, 160, 232, 174, 80, 96, 104, 171, 251, 9, 6, 211, 16, 50, 185, 141, 140, 137, 239, 232, 104, 117, 24, 108, 83, 130, 56, 185, 48, 13, 160, 145, 39, 59, 169, 154, 154, 101, 84, 15, 162, 222, 183, 225, 111, 140, 13, 75, 198, 96, 19, 175, 64, 213, 150, 96, 191, 208, 157, 140, 83, 76, 105, 191, 117, 61, 64, 192, 155, 219, 229, 26, 34, 142, 48, 219, 57, 168, 123, 38, 80, 96, 60, 44, 38, 10, 143, 120, 210, 95, 240, 216, 133, 85, 146, 178, 145, 194, 72, 159, 130, 119, 132, 14, 56, 58, 174, 47, 102, 216, 34, 211, 50, 191, 247, 216, 97, 212, 92, 171, 62, 230, 118, 67, 250, 148, 45, 60, 178, 212, 232, 119, 20, 243, 107, 229, 234, 115, 246, 212, 93, 83, 161, 49, 188, 0, 252, 87, 200, 5, 27, 235, 101, 117, 99, 210, 24, 144, 53, 185, 143, 206, 176, 112, 204, 39, 56, 130, 40, 71, 49, 6, 190, 254, 191, 187, 137, 211, 139, 19, 179, 165, 164, 215, 191, 244, 151, 237, 11, 17, 103, 37, 242, 105, 252, 188, 148, 29, 152, 130, 89, 156, 44, 150, 162, 46, 39, 90, 210, 7, 194, 211, 227, 28, 165, 48, 53, 179, 90, 215, 220, 44, 143, 45, 115, 127, 9, 122, 154, 31, 206, 44, 198, 136, 8, 132, 224, 185, 232, 17, 19, 23, 39, 238, 107, 57, 33, 219, 64, 42, 20, 38, 181, 39, 24, 101, 182, 183, 163, 127, 184, 90, 113, 218, 137, 255, 197, 34, 142, 33, 130, 82, 225, 7, 214, 158, 26, 158, 89, 171, 94, 193, 173, 48, 189, 74, 214, 143, 56, 73, 131, 16, 251, 28, 3, 64, 69, 251, 181, 209, 239, 186, 39, 114, 139, 192, 235, 41, 71, 37, 101, 233, 144, 93, 195, 66, 18, 101, 226, 93, 74, 202, 52, 7, 36, 48, 39, 203, 104, 136, 111, 120, 139, 10, 164, 164, 157, 70, 235, 29, 248, 253, 18, 14, 129, 233, 216, 176, 74, 189, 183, 103, 223, 146, 48, 10, 243, 185, 68, 63, 250, 103, 15, 81, 182, 74, 244, 247, 142, 182, 216, 224, 39, 55, 254, 129, 76, 174, 96, 12, 10, 154, 209, 135, 232, 64, 125, 114, 6, 158, 227, 84, 92, 49, 13, 232, 114, 165, 152, 216, 51, 129, 58, 15, 211, 30, 83, 230, 13, 125, 231, 1, 167, 193, 63, 250, 56, 133, 208, 246, 70, 24, 133, 234, 179, 81, 248];
    const ED25519_SIGNATURE_RESP: [u8; 92] = [0, 0, 0, 88, SshAgentSignResponse as u8, 0, 0, 0, 83, 0, 0, 0, 11, 115, 115, 104, 45, 101, 100, 50, 53, 53, 49, 57, 0, 0, 0, 64, 206, 183, 24, 82, 159, 100, 204, 58, 106, 42, 60, 182, 38, 205, 226, 77, 193, 102, 4, 198, 197, 144, 227, 135, 79, 137, 77, 131, 235, 160, 84, 55, 85, 193, 24, 210, 7, 198, 250, 158, 71, 176, 113, 250, 103, 181, 140, 75, 231, 86, 92, 132, 17, 207, 237, 160, 255, 35, 76, 132, 143, 10, 178, 9];
    const ED25519_SIGNATURE: [u8; 64] = [206, 183, 24, 82, 159, 100, 204, 58, 106, 42, 60, 182, 38, 205, 226, 77, 193, 102, 4, 198, 197, 144, 227, 135, 79, 137, 77, 131, 235, 160, 84, 55, 85, 193, 24, 210, 7, 198, 250, 158, 71, 176, 113, 250, 103, 181, 140, 75, 231, 86, 92, 132, 17, 207, 237, 160, 255, 35, 76, 132, 143, 10, 178, 9];


    #[test]
    fn read_keys_ok() {
        let stream = Cursor::new(KEYS_LIST.to_vec());
        let keys = SshAgentClient::read_keys(stream).unwrap();
        assert_eq!(4, keys.len());
        for key in keys {
            println!("{:?}", key);
            match key.comment.as_str() {
                RSA_COMMENT => {
                    assert_eq!(SshKeyType::RSA, key.key_type);
                    assert_eq!(RSA_KEY.len(), key.public.len());
                    assert_eq!(RSA_MD5.to_vec(), key.md5_fingerprint());
                }
                ED25519_COMMENT => {
                    assert_eq!(SshKeyType::ED25519, key.key_type);
                    assert_eq!(ED25519_KEY.len(), key.public.len());
                    assert_eq!(ED25519_MD5.to_vec(), key.md5_fingerprint());
                    assert_eq!(ED25519_SHA256.to_vec(), key.sha256_fingerprint());
                }
                DSA_COMMENT => {
                    assert_eq!(SshKeyType::DSA, key.key_type);
                    assert_eq!(DSA_KEY.len(), key.public.len());
                }
                ECDSA_COMMENT => {
                    assert_eq!(SshKeyType::ECDSA, key.key_type);
                    assert_eq!(ECDSA__KEY.len(), key.public.len());
                }
                _ => panic!("Unexpected key")
            }
        }
    }

    #[test]
    fn read_keys_one() {
        let stream = Cursor::new(ONE_KEY_LIST.to_vec());
        let mut keys = SshAgentClient::read_keys(stream).unwrap();
        assert_eq!(1, keys.len());
        let ed25519 = keys.pop().unwrap();
        assert_eq!(SshKeyType::ED25519, ed25519.key_type);
        assert_eq!(ED25519_COMMENT, ed25519.comment);
        assert_eq!(ED25519_KEY.len(), ed25519.public.len());
        assert_eq!(ED25519_MD5.to_vec(), ed25519.md5_fingerprint());
    }

    #[test]
    fn read_keys_none() {
        let stream = Cursor::new(vec![0, 0, 0, 5, SshAgentIdentitiesAnswer as u8, 0, 0, 0, 0]);
        let keys = SshAgentClient::read_keys(stream).unwrap();
        assert_eq!(0, keys.len());
    }


    #[test]
    fn read_keys_failure_msg() {
        let stream = Cursor::new(vec![0, 0, 0, 1, 5]);
        match SshAgentClient::read_keys(stream) {
            Ok(_p) => panic!("Failure message expected"),
            Err(e) => println!("Got error as expected {:?}", e)
        }
    }

    #[test]
    fn read_keys_empty_key() {
        let stream = Cursor::new(vec![0, 0, 0, 9, SshAgentIdentitiesAnswer as u8, 0, 0, 0, 1, 0, 0, 0, 0]);
        match SshAgentClient::read_keys(stream) {
            Ok(_p) => panic!("Failure message expected"),
            Err(e) => println!("Got error as expected {:?}", e)
        }
    }

    #[test]
    fn list_keys_request_message() {
        let message: Vec<u8> = SshAgentClient::list_message().into();
        let expected: Vec<u8> = vec![0, 0, 0, 1, 11];
        assert_eq!(expected, message);
    }

    #[test]
    fn sign_request_message() {
        let data: Vec<u8> = vec![0, 1, 2, 3];
        let stream = Cursor::new(ED25519_RAW_KEY.to_vec());
        let key = SshKey::read(stream).unwrap();
        let message: Vec<u8> = SshAgentClient::sign_message(data.as_ref(), &key).unwrap().into();
        let mut expected: Vec<u8> = vec![0, 0, 0, 68, 13];
        expected.extend(vec![0, 0, 0, 51]);               // key size
        expected.extend_from_slice(ED25519_KEY.as_ref()); // key
        expected.extend(vec![0, 0, 0, 4, 0, 1, 2, 3]);     // data
        expected.extend(vec![0, 0, 0, 0]);                 // flags
        assert_eq!(expected, message);
    }

    #[test]
    fn read_signed_ed25519() {
        let signature_response = Cursor::new(ED25519_SIGNATURE_RESP.to_vec());
        let signature = SshAgentClient::read_signed(signature_response).unwrap();
        assert_eq!(SshKeyType::ED25519, signature.signature_type);
        assert_eq!(ED25519_SIGNATURE.to_vec(), signature.signature);
    }

    #[test]
    fn read_signed_rsa() {
        let signature_response = Cursor::new(RSA_SIGNATURE_RESP.to_vec());
        let signature = SshAgentClient::read_signed(signature_response).unwrap();
        assert_eq!(SshKeyType::RSA, signature.signature_type);
        assert_eq!(RSA_SIGNATURE.to_vec(), signature.signature);
    }

    #[test]
    fn read_signed_wrong_response() {
        let signature_response = Cursor::new(vec![0u8, 0, 0, 9, SshAgentSuccess as u8, 0, 0, 0, 4, 1, 2, 3, 4]);
        match SshAgentClient::read_signed(signature_response) {
            Ok(_p) => panic!("Error expected"),
            Err(e) => println!("Got error as expected {:?}", e)
        }
    }

    #[test]
    fn read_signed_failed() {
        let signature_response = Cursor::new(vec![0u8, 0, 0, 1, SshAgentFailure as u8]);
        match SshAgentClient::read_signed(signature_response) {
            Ok(_p) => panic!("Failure message expected"),
            Err(e) => println!("Got error as expected {:?}", e)
        }
    }

    #[test]
    fn find_key_store_empty() {
        let key_store = SshKeyStore::new(None);
        assert_eq!(None, key_store.find_key(KeySelector::Comment(ED25519_COMMENT.to_string()).borrow()));
    }

    #[test]
    fn find_key_by_comment_ok() {
        let keys = vec![SshKey::new(SshKeyType::ED25519, ED25519_COMMENT.to_string(), ED25519_KEY.to_vec(), None)];
        let key_store = SshKeyStore::new(Some(keys));
        match key_store.find_key(KeySelector::Comment(ED25519_COMMENT.to_string()).borrow()) {
            Some(key) => {
                assert_eq!(ED25519_COMMENT, key.comment);
                assert_eq!(SshKeyType::ED25519, key.key_type);
                assert_eq!(ED25519_KEY.len(), key.public.len());
                assert_eq!(ED25519_MD5.to_vec(), key.md5_fingerprint());
                assert_eq!(None, key.private);
            }
            None => panic!("Key should be found")
        }
    }

    #[test]
    fn find_key_by_comment_not_found() {
        let keys = vec![SshKey::new(SshKeyType::RSA, RSA_COMMENT.to_string(), RSA_KEY.to_vec(), None)];
        let key_store = SshKeyStore::new(Some(keys));
        match key_store.find_key(KeySelector::Comment(ED25519_COMMENT.to_string()).borrow()) {
            Some(key) => panic!("Key '{:?}' should not be found", key),
            None => println!("Key should not found as expected")
        }
    }

    #[test]
    fn find_key_by_md5_ok() {
        let keys = vec![SshKey::new(SshKeyType::ED25519, ED25519_COMMENT.to_string(), ED25519_KEY.to_vec(), None)];
        let key_store = SshKeyStore::new(Some(keys));
        match key_store.find_key(KeySelector::MD5Fingerprint(ED25519_MD5.to_vec()).borrow()) {
            Some(key) => {
                assert_eq!(ED25519_COMMENT, key.comment);
                assert_eq!(SshKeyType::ED25519, key.key_type);
                assert_eq!(ED25519_KEY.len(), key.public.len());
                assert_eq!(ED25519_MD5.to_vec(), key.md5_fingerprint());
                assert_eq!(ED25519_SHA256.to_vec(), key.sha256_fingerprint());
                assert_eq!(None, key.private);
            }
            None => panic!("Key should be found")
        }
    }

    #[test]
    fn find_key_by_md5_not_found() {
        let keys = vec![SshKey::new(SshKeyType::RSA, RSA_COMMENT.to_string(), RSA_KEY.to_vec(), None)];
        let key_store = SshKeyStore::new(Some(keys));
        match key_store.find_key(KeySelector::MD5Fingerprint(ED25519_MD5.to_vec()).borrow()) {
            Some(key) => panic!("Key '{:?}' should not be found", key),
            None => println!("Key should not found as expected")
        }
    }

    #[test]
    fn find_key_by_sha256_ok() {
        let keys = vec![SshKey::new(SshKeyType::ED25519, ED25519_COMMENT.to_string(), ED25519_KEY.to_vec(), None)];
        let key_store = SshKeyStore::new(Some(keys));
        match key_store.find_key(KeySelector::SHA256Fingerprint(ED25519_SHA256.to_vec()).borrow()) {
            Some(key) => {
                assert_eq!(ED25519_COMMENT, key.comment);
                assert_eq!(SshKeyType::ED25519, key.key_type);
                assert_eq!(ED25519_KEY.len(), key.public.len());
                assert_eq!(ED25519_MD5.to_vec(), key.md5_fingerprint());
                assert_eq!(ED25519_SHA256.to_vec(), key.sha256_fingerprint());
                assert_eq!(None, key.private);
            }
            None => panic!("Key should be found")
        }
    }

    #[test]
    fn find_key_by_sha256_not_found() {
        let keys = vec![SshKey::new(SshKeyType::RSA, RSA_COMMENT.to_string(), RSA_KEY.to_vec(), None)];
        let key_store = SshKeyStore::new(Some(keys));
        match key_store.find_key(KeySelector::SHA256Fingerprint(ED25519_SHA256.to_vec()).borrow()) {
            Some(key) => panic!("Key '{:?}' should not be found", key),
            None => println!("Key should not found as expected")
        }
    }

    #[test]
    fn ssh_key_type_from() {
        assert_eq!(SshKeyType::RSA, SshKeyType::from(RSA_TYPE_NAME));
        assert_eq!(SshKeyType::ED25519, SshKeyType::from(ED25519_TYPE_NAME));
        assert_eq!(SshKeyType::ECDSA, SshKeyType::from(ECDSA_TYPE_NAME));
        assert_eq!(SshKeyType::DSA, SshKeyType::from(DSA_TYPE_NAME));
        assert_eq!(SshKeyType::UNKNOWN, SshKeyType::from("ssh"));
    }

    // #[test]
    fn real_agent() {
        let message = vec![0u8, 1, 2, 3, 4, 5, 6, 7, 8];
        let comment_ed25519 = "gert@hobbton.be".to_string();
        let md5_ed25519: [u8; 16] = [0xd0, 0x34, 0x91, 0xab, 0x3c, 0x55, 0x30, 0x5e, 0x79, 0xca, 0xd4, 0xad, 0x8f, 0xe6, 0x96, 0x60];
        let sha256_ed25519: [u8; 32] = [0x07, 0x76, 0xe0, 0x07, 0xd9, 0x31, 0x34, 0x5f, 0x8a, 0x44, 0xb1, 0xec, 0xd7, 0x40, 0xb9, 0x58, 0xe4, 0xcd, 0x7d, 0x9a, 0x3d, 0xcc, 0x36, 0xd6, 0xb2, 0xcf, 0x2f, 0xfe, 0xcb, 0x95, 0x9a, 0x64];
        let mut signed_ed25519 = SshSignature {
            signature_type: SshKeyType::UNKNOWN,
            signature: vec![],
        };
        let mut agent = SshAgentClient::connect().unwrap();
        let keys = agent.list_keys().unwrap();
        for key in keys {
            println!("{:?}", key);
            let sign_msg: Vec<u8> = SshAgentClient::sign_message(message.as_slice(), &key).unwrap().into();
            println!("Signature Msg {:?}", sign_msg);
            agent.connection.write_all(sign_msg.as_slice()).unwrap();
            let signed = SshAgentClient::read_signed(&agent.connection).unwrap();
            println!("{:?}", signed);
            if key.comment == comment_ed25519 {
                signed_ed25519 = signed;
            }
        }

        match agent.sign(message.as_slice(), KeySelector::Comment(ED25519_COMMENT.to_string())) {
            Ok(_signed) => panic!("Not expecting key presence"),
            Err(e) => println!("Expecting error {}", e)
        }

        match agent.sign(message.as_slice(), KeySelector::Comment(comment_ed25519)) {
            Ok(signed) => {
                println!("ED25519 from comment Signed {:?}", signed);
                assert_eq!(signed_ed25519, signed);
            }
            Err(e) => panic!("Unxpected error {:?}", e)
        }

        match agent.sign(message.as_slice(), KeySelector::MD5Fingerprint(ED25519_MD5.to_vec())) {
            Ok(_signed) => panic!("Not expecting key presence"),
            Err(e) => println!("Expecting error {}", e)
        }

        match agent.sign(message.as_slice(), KeySelector::MD5Fingerprint(md5_ed25519.to_vec())) {
            Ok(signed) => {
                println!("ED25519 from md5 Signed {:?}", signed);
                assert_eq!(signed_ed25519, signed);
            }
            Err(e) => panic!("Unxpected error {:?}", e)
        }

        match agent.sign(message.as_slice(), KeySelector::SHA256Fingerprint(ED25519_SHA256.to_vec())) {
            Ok(_signed) => panic!("Not expecting key presence"),
            Err(e) => println!("Expecting error {}", e)
        }

        match agent.sign(message.as_slice(), KeySelector::SHA256Fingerprint(sha256_ed25519.to_vec())) {
            Ok(signed) => {
                println!("ED25519 from md5 Signed {:?}", signed);
                assert_eq!(signed_ed25519, signed);
            }
            Err(e) => panic!("Unxpected error {:?}", e)
        }
    }
}
